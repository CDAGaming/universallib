package com.gitlab.cdagaming.universallib;

import com.gitlab.cdagaming.universallib.api.ConfigInstance;
import com.gitlab.cdagaming.universallib.api.GameListener;
import com.gitlab.cdagaming.universallib.api.ModInstance;
import com.gitlab.cdagaming.universallib.api.functions.UpdateChecker;
import com.gitlab.cdagaming.universallib.config.ConfigHandler;
import com.gitlab.cdagaming.universallib.config.FrameworkProperties;
import com.gitlab.cdagaming.universallib.handler.FileHandler;
import com.google.common.collect.Lists;
import net.minecraftforge.fml.common.Mod;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Mod(modid = FrameworkConstants.MODID, name = FrameworkConstants.NAME, version = FrameworkConstants.VERSION_ID, clientSideOnly = true, guiFactory = FrameworkConstants.GUI_FACTORY, canBeDeactivated = true, updateJSON = FrameworkConstants.UPDATE_JSON, certificateFingerprint = FrameworkConstants.FINGERPRINT, acceptedMinecraftVersions = "*")
public class UniversalLib {
    public static ConfigHandler CONFIG;
    public static FrameworkProperties PROPERTIES;
    public static Timer timerObj = new Timer(FrameworkConstants.NAME);
    public static List<ModInstance> modInstances = Lists.newArrayList();
    public static List<GameListener> modListeners = Lists.newArrayList();
    public static List<ConfigInstance> configInstances = Lists.newArrayList();

    public UniversalLib() {
        scheduleTick();
    }

    private void scheduleTick() {
        if (!FrameworkConstants.closing) {
            try {
                timerObj.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                clientTick();
                            }
                        },
                        50
                );
            } catch (Exception ex) {
                shutDown();
            } catch (Error er) {
                shutDown();
            }
        }
    }

    private void clientTick() {
        if (!FrameworkConstants.isInitialized) {
            // Initialize a Shutdown Thread for when JVM Shuts down
            Thread shutdownThread = new Thread("UniversalLib-ShutDown-Handler") {
                @Override
                public void run() {
                    if (!FrameworkConstants.closing) {
                        shutDown();
                    }
                }
            };
            shutdownThread.setDaemon(true);
            Runtime.getRuntime().addShutdownHook(shutdownThread);

            // Check for Updates for Main Library, if Forge is not Set as the ModLoader
            if (!FrameworkConstants.MODLOADER.toLowerCase().contains("forge") && !FrameworkConstants.BRAND.toLowerCase().contains("forge")) {
                UpdateChecker.checkForUpdates(FrameworkConstants.UPDATE_JSON, FrameworkConstants.MODID, FrameworkConstants.VERSION_ID);
            }

            // Create List of Search Events
            List<Class<?>> searchClasses = Lists.newArrayList(ModInstance.class, GameListener.class, ConfigInstance.class);

            // Scan for valid Mod Instances and Search Events
            FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.info.scan.start"));
            for (Class<?> modInstanceClass : FileHandler.getClassNamesMatchingSuperType(searchClasses)) {
                if (modInstanceClass != null) {
                    List<Class<?>> interfacesList = Arrays.asList(modInstanceClass.getInterfaces());

                    try {
                        if (interfacesList.contains(ModInstance.class)) {
                            FrameworkConstants.LOG.info("Mod Class Found: " + modInstanceClass.getSimpleName());
                            ModInstance modInstance = (ModInstance) modInstanceClass.newInstance();
                            modInstances.add(modInstance);

                            // Set Mod Instance Event
                            modInstance.setInstance(modInstance);
                        }
                        if (interfacesList.contains(GameListener.class)) {
                            FrameworkConstants.LOG.info("GameListener Class Found: " + modInstanceClass.getSimpleName());
                            GameListener gameListener = (GameListener) modInstanceClass.newInstance();
                            modListeners.add(gameListener);
                        }
                        if (interfacesList.contains(ConfigInstance.class)) {
                            FrameworkConstants.LOG.info("Config Class Found: " + modInstanceClass.getSimpleName());
                            ConfigInstance configInstance = (ConfigInstance) modInstanceClass.newInstance();
                            configInstances.add(configInstance);
                        }
                    } catch (Exception ignored) {
                        // Ignore Class Exceptions
                    }
                }
            }

            // Initialize Main Config and Character Data FIRST
            PROPERTIES = new FrameworkProperties();
            CONFIG = new ConfigHandler(FrameworkConstants.MODID, FrameworkConstants.configDir + File.separator + FrameworkConstants.MODID + ".properties", "/assets/" + FrameworkConstants.MODID + "/config.properties", FrameworkConstants.TRANSLATOR, PROPERTIES);
            CONFIG.initialize();
            
            FrameworkConstants.loadCharData(!FrameworkConstants.MAIN_LIB_DIR.exists() || FrameworkConstants.MAIN_LIB_DIR.listFiles() == null);

            // Initialize all other Mod Configs
            for (ConfigInstance configInstance : configInstances) {
                if (!configInstance.getModInstance().isClosingOrHasError()) {
                    ConfigHandler configHandler = new ConfigHandler(configInstance.getModInstance().getID(), configInstance.configFilePath(), configInstance.configDataLocalPath(), configInstance.getModInstance().getTranslatorInstance(), configInstance.getClassInstance());
                    configInstance.getModInstance().setConfig(configHandler);
                    configHandler.initialize();
                }
            }

            if (!modInstances.isEmpty()) {
                FrameworkConstants.isInitialized = true;
                scheduleTick();
            } else {
                FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.info.scan.none"));
                shutDown();
            }
        } else if (!FrameworkConstants.closing) {
            // Update Instance Data and Tick through Primary Events
            FrameworkConstants.updateInstanceData();
            FrameworkConstants.SYSTEM.tick();
            FrameworkConstants.TRANSLATOR.tick();

            // Mod Instance Checks (Only done after Library Mod is fully initialized)
            for (ModInstance modInstance : modInstances) {
                try {
                    if (modInstance.isInitialized()) {
                        // Refresh Translators and Do a Tick for each Instance
                        modInstance.getTranslatorInstance().tick();
                        modInstance.clientTick();
                    } else if (modInstance.isClosingOrHasError()) {
                        // Shut Down only the Mod Instance that is causing Errors or is Closing
                        modInstance.shutDown();
                    } else {
                        if (!FrameworkConstants.MODLOADER.toLowerCase().contains("forge") && !FrameworkConstants.BRAND.toLowerCase().contains("forge")) {
                            // Check for Updates for Each Mod, then Initialize Mod - TODO: Add GUI Flag, don't initialize mod until GUI dismissal
                            UpdateChecker.checkForUpdates(modInstance.getUpdateJSON(), modInstance.getID(), modInstance.getVersion());
                        }
                        modInstance.modInit();
                    }
                } catch (Exception ex) {
                    modInstance.shutDown();
                } catch (Error er) {
                    modInstance.shutDown();
                }
            }

            // Tick through Game Events
            FrameworkConstants.GUI_HANDLER.onTick();

            // Schedule Next Tick
            scheduleTick();
        } else {
            shutDown();
        }
    }

    private void shutDown() {
        FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate("universallib.logger.info.shutdown", FrameworkConstants.MODID));
        FrameworkConstants.closing = true;
        timerObj.cancel();

        // Do Shutdown on all Mod Instances
        for (ModInstance modInstance : modInstances) {
            if (!modInstance.isClosingOrHasError()) {
                FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate("universallib.logger.info.shutdown", modInstance.getID()));
                modInstance.shutDown();
            }
        }
    }
}
