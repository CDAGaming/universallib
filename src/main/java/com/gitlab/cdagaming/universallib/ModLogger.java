package com.gitlab.cdagaming.universallib;

import com.gitlab.cdagaming.universallib.handler.StringHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModLogger {
    private String loggerName;
    private Logger logInstance;

    public ModLogger(final String loggerName) {
        this.loggerName = loggerName;
        this.logInstance = LogManager.getLogger(loggerName);
    }

    public void error(final String logMessage, Object... logArguments) {
        if (FrameworkConstants.playerInstance != null && !UniversalLib.CONFIG.hasChanged && !FrameworkConstants.closing && UniversalLib.PROPERTIES.showLoggingInChat) {
            StringHandler.sendMessageToPlayer(FrameworkConstants.playerInstance, "§6§l[§f§l" + loggerName + "§6]§r§c " + logMessage);
        } else {
            logInstance.error(logMessage, logArguments);
        }
    }

    public void warn(final String logMessage, Object... logArguments) {
        if (FrameworkConstants.playerInstance != null && !UniversalLib.CONFIG.hasChanged && !FrameworkConstants.closing && UniversalLib.PROPERTIES.showLoggingInChat) {
            StringHandler.sendMessageToPlayer(FrameworkConstants.playerInstance, "§6§l[§f§l" + loggerName + "§6]§r§e " + logMessage);
        } else {
            logInstance.warn(logMessage, logArguments);
        }
    }

    public void info(final String logMessage, Object... logArguments) {
        if (FrameworkConstants.playerInstance != null && !UniversalLib.CONFIG.hasChanged && !FrameworkConstants.closing && UniversalLib.PROPERTIES.showLoggingInChat) {
            StringHandler.sendMessageToPlayer(FrameworkConstants.playerInstance, "§6§l[§f§l" + loggerName + "§6]§r " + logMessage);
        } else {
            logInstance.info(logMessage, logArguments);
        }
    }
}
