package com.gitlab.cdagaming.universallib.api;

public interface ConfigInstance {
    void onInit();

    void onRead();

    void onUpdate();

    void validationEvent();

    void transferEvent();

    ModInstance getModInstance();

    String configDataLocalPath();

    String configFilePath();

    Object getClassInstance();
}
