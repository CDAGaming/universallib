package com.gitlab.cdagaming.universallib.api.listeners;

public interface TimerEventListener {
    void execute();
}
