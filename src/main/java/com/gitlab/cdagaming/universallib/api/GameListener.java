package com.gitlab.cdagaming.universallib.api;

import java.util.List;

public interface GameListener {
    void onTick();

    void onClearClientData();

    void onClearFullData();

    String eventID();

    boolean requiresDataSupply();

    List<String> getListAdditions();
}
