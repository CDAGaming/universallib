package com.gitlab.cdagaming.universallib.api.functions;

import com.gitlab.cdagaming.universallib.FrameworkConstants;
import com.gitlab.cdagaming.universallib.handler.StringHandler;
import com.gitlab.cdagaming.universallib.handler.URLHandler;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

public class UpdateChecker {

    public static JsonObject updateObject;

    public static void checkForUpdates(final String updateURL, final String modID, final String versionID) {
        try {
            FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.info.update.checking"));
            updateObject = URLHandler.getJSONFromURL(updateURL, JsonObject.class);
            if (updateObject != null) {
                String latestVersion = updateObject.get("promos").getAsJsonObject().get(FrameworkConstants.MCVersion + "-latest").getAsString();
                if (compare(versionID, latestVersion) == -1) {
                    // Get Changelog Data and Open Update GUI if an Update is available
                    JsonElement changelogRawData = updateObject.get(FrameworkConstants.MCVersion).getAsJsonObject().get(latestVersion);

                    List<String> changelogData = Lists.newArrayList();
                    if (changelogRawData != null && !StringHandler.isNullOrEmpty(changelogRawData.toString())) {
                        changelogData = StringHandler.splitTextByNewLine(changelogRawData.toString());
                    }

                    // Put in Logging and Open Update GUI - TODO
                    FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.info.update.available"));
                } else {
                    FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.info.update.none"));
                }
            }
        } catch (Exception ex) {
            // Unable to Check - TODO
            ex.printStackTrace();
        }
    }

    /**
     * Compares one version string to another version string by dotted ordinals.
     * eg. "1.0" > "0.09" ; "0.9.5" < "0.10",
     * also "1.0" < "1.0.0" but "1.0" == "01.00"
     *
     * @param left  the left hand version string
     * @param right the right hand version string
     * @return 0 if equal, -1 if right > left and 1 otherwise.
     */
    public static int compare(String left, String right) {
        // Remove all Letters from both Versions, just in case
        left = left.replaceAll("[a-zA-Z]", "");
        right = right.replaceAll("[a-zA-Z]", "");

        if (left.equals(right)) {
            return 0;
        }
        int leftStart = 0, rightStart = 0, result;
        do {
            int leftEnd = left.indexOf('.', leftStart);
            int rightEnd = right.indexOf('.', rightStart);
            Integer leftValue = Integer.parseInt(leftEnd < 0
                    ? left.substring(leftStart)
                    : left.substring(leftStart, leftEnd));
            Integer rightValue = Integer.parseInt(rightEnd < 0
                    ? right.substring(rightStart)
                    : right.substring(rightStart, rightEnd));
            result = leftValue.compareTo(rightValue);
            leftStart = leftEnd + 1;
            rightStart = rightEnd + 1;
        } while (result == 0 && leftStart > 0 && rightStart > 0);
        if (result == 0) {
            if (leftStart > rightStart) {
                return containsNonZeroValue(left, leftStart) ? 1 : 0;
            }
            if (leftStart < rightStart) {
                return containsNonZeroValue(right, rightStart) ? -1 : 0;
            }
        }
        return result;
    }

    private static boolean containsNonZeroValue(String str, int beginIndex) {
        for (int i = beginIndex; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c != '0' && c != '.') {
                return true;
            }
        }
        return false;
    }
}
