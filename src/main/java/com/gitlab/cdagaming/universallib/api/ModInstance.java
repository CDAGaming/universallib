package com.gitlab.cdagaming.universallib.api;

import com.gitlab.cdagaming.universallib.api.listeners.TimerEventListener;
import com.gitlab.cdagaming.universallib.config.ConfigHandler;
import com.gitlab.cdagaming.universallib.handler.TranslationHandler;

import java.util.HashMap;

public interface ModInstance {
    void clientTick();

    void modInit();

    void shutDown();

    void setConfig(ConfigHandler configInstance);

    void setInstance(ModInstance modInstance);

    TranslationHandler getTranslatorInstance();

    boolean isInitialized();

    boolean isClosingOrHasError();

    String getID();

    String getVersion();

    String getUpdateJSON();

    HashMap<Integer, TimerEventListener> loopSecondEvents();
}
