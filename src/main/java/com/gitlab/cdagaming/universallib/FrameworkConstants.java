package com.gitlab.cdagaming.universallib;

import com.gitlab.cdagaming.universallib.handler.FileHandler;
import com.gitlab.cdagaming.universallib.handler.StringHandler;
import com.gitlab.cdagaming.universallib.handler.SystemHandler;
import com.gitlab.cdagaming.universallib.handler.TranslationHandler;
import com.gitlab.cdagaming.universallib.handler.gui.GUIHandler;
import com.google.common.collect.Lists;
import net.minecraft.client.ClientBrandRetriever;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class FrameworkConstants {
    public static final String NAME = "GRADLE:mod_name";
    public static final String majorVersion = "GRADLE:majorVersion";
    public static final String minorVersion = "GRADLE:minorVersion";
    public static final String revisionVersion = "GRADLE:revisionVersion";
    public static final String VERSION_ID = "v" + majorVersion + "." + minorVersion + "." + revisionVersion;
    public static final String MODID = "GRADLE:mod_id";
    public static final String GUI_FACTORY = "com.gitlab.cdagaming." + MODID + ".config.ModGUIFactory";
    public static final String MCVersion = "GRADLE:mc_Version";
    public static final String MODLOADER = "GRADLE:modloader";
    public static final String BRAND = ClientBrandRetriever.getClientModName();
    public static final SystemHandler SYSTEM = new SystemHandler();
    public static final String configDir = SYSTEM.USER_DIR + File.separator + "config";
    public static final String modsDir = SYSTEM.USER_DIR + File.separator + "mods";
    public static final String UPDATE_JSON = "https://gitlab.com/CDAGaming/VersionLibrary/raw/master/" + MODID + "/update.json";
    public static final String FINGERPRINT = "GRADLE:certFingerprint";
    public static final ModLogger LOG = new ModLogger(MODID);
    public static final ClassLoader CLASS_LOADER = Thread.currentThread().getContextClassLoader();
    public static final TranslationHandler TRANSLATOR = new TranslationHandler(MODID, false);
    public static final File MAIN_LIB_DIR = new File("cdagaming" + File.separator + MODID);
    public static final GUIHandler GUI_HANDLER = new GUIHandler();
    public static boolean forceBlockTooltipRendering = false, isInitialized = false, closing = false;
    public static Minecraft minecraftInstance = Minecraft.getMinecraft();
    public static final String USERNAME = minecraftInstance.getSession().getUsername();
    public static EntityPlayer playerInstance = minecraftInstance.player;

    public static void updateInstanceData() {
        minecraftInstance = Minecraft.getMinecraft();
        playerInstance = minecraftInstance.player;
    }

    public static void loadCharData(final boolean Update) {
        LOG.info(TRANSLATOR.translate(true, "universallib.logger.info.chardata.init"));
        final String fileName = "chardata.properties", charDataPath = "/assets/" + MODID + "/" + fileName;
        final File charDataDir = new File(MAIN_LIB_DIR + File.separator + fileName);
        boolean UpdateStatus = Update || !charDataDir.exists(), errored = false;
        InputStream inputData = null;
        InputStreamReader inputStream = null;
        OutputStream outputData = null;
        BufferedReader reader = null;

        if (UpdateStatus) {
            LOG.info(TRANSLATOR.translate(true, "universallib.logger.info.download.init", fileName, charDataDir.getAbsolutePath(), charDataPath));
            inputData = StringHandler.getResourceAsStream(FrameworkConstants.class, charDataPath);

            // Write Data from Local charData to Directory if Update is needed
            if (inputData != null) {
                try {
                    outputData = FileHandler.getOutputStream(charDataDir);

                    byte[] transferBuffer = new byte[inputData.available()];
                    for (int readBuffer = inputData.read(transferBuffer); readBuffer != -1; readBuffer = inputData.read(transferBuffer)) {
                        outputData.write(transferBuffer, 0, readBuffer);
                    }

                    LOG.info(TRANSLATOR.translate(true, "universallib.logger.info.download.loaded", fileName, charDataDir.getAbsolutePath(), charDataPath));
                } catch (Exception ex) {
                    errored = true;
                    ex.printStackTrace();
                }
            } else {
                errored = true;
            }
        }

        if (!errored) {
            try {
                inputData = new FileInputStream(charDataDir);
                inputStream = new InputStreamReader(inputData, Charset.forName("UTF-8"));
                reader = new BufferedReader(inputStream);

                String currentString;
                while ((currentString = reader.readLine()) != null) {
                    String[] localWidths;
                    currentString = currentString.trim();

                    if (!currentString.startsWith("=") && currentString.contains("=")) {
                        String[] splitString = currentString.split("=", 2);

                        if (splitString[0].equalsIgnoreCase("charWidth")) {
                            localWidths = splitString[1].replaceAll("\\[", "").replaceAll("]", "").split(", ");

                            for (int i = 0; i < localWidths.length && i <= 256; i++) {
                                StringHandler.MC_CHAR_WIDTH[i] = Integer.parseInt(localWidths[i].trim());
                            }
                        } else if (splitString[0].equalsIgnoreCase("glyphWidth")) {
                            localWidths = splitString[1].replaceAll("\\[", "").replaceAll("]", "").split(", ");

                            for (int i = 0; i < localWidths.length && i <= 65536; i++) {
                                StringHandler.MC_GLYPH_WIDTH[i] = Byte.parseByte(localWidths[i].trim());
                            }
                        }
                    }
                }

                if (Arrays.equals(StringHandler.MC_CHAR_WIDTH, new int[256]) || Arrays.equals(StringHandler.MC_GLYPH_WIDTH, new byte[65536])) {
                    errored = true;
                }
            } catch (Exception ex) {
                loadCharData(true);
            }
        }

        try {
            if (reader != null) {
                reader.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (inputData != null) {
                inputData.close();
            }
            if (outputData != null) {
                outputData.close();
            }
        } catch (Exception ex) {
            LOG.error(TRANSLATOR.translate(true, "universallib.logger.error.dataclose"));
            ex.printStackTrace();
        } finally {
            if (errored) {
                LOG.error(TRANSLATOR.translate(true, "universallib.logger.error.chardata"));
                forceBlockTooltipRendering = true;
            } else {
                LOG.info(TRANSLATOR.translate(true, "universallib.logger.info.chardata.loaded"));
                forceBlockTooltipRendering = false;
            }
        }
    }

    public static void writeToCharData() {
        List<String> textData = Lists.newArrayList();
        InputStream inputData = null;
        InputStreamReader inputStream = null;
        OutputStream outputData = null;
        OutputStreamWriter outputStream = null;
        BufferedReader br = null;
        BufferedWriter bw = null;
        final File charDataDir = new File(MAIN_LIB_DIR + File.separator + "chardata.properties");

        if (charDataDir.exists()) {
            try {
                // Read and Queue Character Data
                inputData = new FileInputStream(charDataDir);
                inputStream = new InputStreamReader(inputData, Charset.forName("UTF-8"));
                br = new BufferedReader(inputStream);

                String currentString;
                while (!StringHandler.isNullOrEmpty((currentString = br.readLine()))) {
                    if (currentString.contains("=")) {
                        if (currentString.toLowerCase().startsWith("charwidth")) {
                            textData.add("charWidth=" + Arrays.toString(StringHandler.MC_CHAR_WIDTH));
                        } else if (currentString.toLowerCase().startsWith("glyphwidth")) {
                            textData.add("glyphWidth=" + Arrays.toString(StringHandler.MC_GLYPH_WIDTH));
                        }
                    }
                }

                // Write Queued Character Data
                outputData = FileHandler.getOutputStream(charDataDir);
                outputStream = new OutputStreamWriter(outputData, Charset.forName("UTF-8"));
                bw = new BufferedWriter(outputStream);

                if (!textData.isEmpty()) {
                    for (String lineInput : textData) {
                        bw.write(lineInput);
                        bw.newLine();
                    }
                } else {
                    // If charWidth and glyphWidth don't exist, Reset Character Data
                    loadCharData(true);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (br != null) {
                        br.close();
                    }
                    if (bw != null) {
                        bw.close();
                    }
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    if (inputData != null) {
                        inputData.close();
                    }
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    if (outputData != null) {
                        outputData.close();
                    }
                } catch (Exception ex) {
                    LOG.error(TRANSLATOR.translate(true, "craftpresence.logger.error.dataclose"));
                    ex.printStackTrace();
                }
            }
        } else {
            loadCharData(true);
        }
    }
}
