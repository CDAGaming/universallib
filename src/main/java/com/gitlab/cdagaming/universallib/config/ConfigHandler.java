package com.gitlab.cdagaming.universallib.config;

import com.gitlab.cdagaming.universallib.FrameworkConstants;
import com.gitlab.cdagaming.universallib.UniversalLib;
import com.gitlab.cdagaming.universallib.api.ConfigInstance;
import com.gitlab.cdagaming.universallib.api.functions.Quadruplet;
import com.gitlab.cdagaming.universallib.handler.FileHandler;
import com.gitlab.cdagaming.universallib.handler.StringHandler;
import com.gitlab.cdagaming.universallib.handler.TranslationHandler;
import com.google.common.collect.Lists;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

public class ConfigHandler {
    // CLASS-SPECIFIC - PUBLIC
    public boolean hasChanged = false, hasClientPropertiesChanged = false;
    public File configFile, parentDir;
    public Properties properties = new Properties();
    // CLASS-SPECIFIC - PRIVATE
    // Mappings Syntax: unTranslated Name Field, translated Name Field, Value Type, Value
    private List<Quadruplet<String, String, ? extends Class<?>, String>> configValueMappings = Lists.newArrayList();
    private String fileName, modID, configDataLocalPath;
    private TranslationHandler translatorInstance;
    private Object classReference;
    private boolean verified = false, initialized = false, isConfigNew = false;

    public ConfigHandler(final String modID, final String fileName, final String configDataLocalPath, final TranslationHandler translatorInstance, final Object classReference) {
        this.fileName = !StringHandler.isNullOrEmpty(fileName) ? fileName : FrameworkConstants.configDir + File.separator + modID + ".properties";
        this.modID = modID;
        this.translatorInstance = translatorInstance;
        this.classReference = classReference;

        this.configDataLocalPath = !StringHandler.isNullOrEmpty(configDataLocalPath) ? configDataLocalPath : "/assets/" + modID + "/config.properties";
    }

    public void setupInitialValues() {
        InputStream inputData = null;
        InputStreamReader inputStream = null;
        BufferedReader reader = null;

        configValueMappings.clear();
        try {
            // Read Data from Local Config Resource
            inputData = StringHandler.getResourceAsStream(classReference.getClass(), configDataLocalPath);
            inputStream = new InputStreamReader(inputData, Charset.forName("UTF-8"));
            reader = new BufferedReader(inputStream);

            String currentString;
            while ((currentString = reader.readLine()) != null) {
                currentString = currentString.trim();

                if (!currentString.startsWith("=") && !currentString.startsWith("#") && !currentString.startsWith("//") && currentString.contains("=")) {
                    String[] propertyArray = currentString.split("=", 2);

                    // Add Data to Initial Mappings
                    configValueMappings.add(new Quadruplet<>(propertyArray[0],
                            StringHandler.formatToID(translatorInstance.translate(true, modID + ".gui.config.name." + propertyArray[0].replace("NAME_", "").toLowerCase()), true, false),
                            classReference.getClass().getDeclaredField(propertyArray[0].replace("NAME_", "")).getType(),
                            propertyArray[1]));

                    // Set Field from Property
                    FileHandler.setField(classReference, propertyArray[0].replace("NAME_", ""), propertyArray[1]);
                }
            }
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.config.save", modID));
            ex.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (inputData != null) {
                    inputData.close();
                }
            } catch (Exception ex) {
                FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.dataclose"));
                ex.printStackTrace();
            } finally {
                for (ConfigInstance configInstance : UniversalLib.configInstances) {
                    configInstance.onInit();
                }
                initialized = true;
            }
        }
    }

    public void initialize() {
        try {
            configFile = new File(fileName);
            parentDir = configFile.getParentFile();
            isConfigNew = (!parentDir.exists() && parentDir.mkdirs()) || (!configFile.exists() && configFile.createNewFile());
            setupInitialValues();
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.config.save", modID));
            ex.printStackTrace();
        } finally {
            if (initialized) {
                if (isConfigNew) {
                    updateConfig();
                }
                read(false);
            }
        }
    }

    public void read(final boolean skipLogging) {
        Reader configReader = null;
        FileInputStream inputStream = null;
        verified = false;

        try {
            inputStream = new FileInputStream(configFile);
            configReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            properties.load(configReader);
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.config.save", modID));
            ex.printStackTrace();
        } finally {
            try {
                // Read Each Property from Mappings
                int currentIndex = 0;
                for (Quadruplet<String, String, ? extends Class<?>, String> propertySet : configValueMappings) {
                    // If Property Value is different from Mappings Value, and Config is not New, update property
                    if (!classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", "")).get(classReference).toString().equals(propertySet.getFourth())) {
                        if (propertySet.getThird().equals(Boolean.TYPE) || propertySet.getThird().equals(boolean.class)) {
                            FileHandler.setField(classReference, propertySet.getFirst().replace("NAME_", ""), StringHandler.isValidBoolean(properties.getProperty(propertySet.getSecond())) ? Boolean.parseBoolean(properties.getProperty(propertySet.getSecond())) : Boolean.parseBoolean(classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", "")).get(classReference).toString()));
                        } else if (propertySet.getThird().equals(Double.TYPE) || propertySet.getThird().equals(double.class)) {
                            FileHandler.setField(classReference, propertySet.getFirst().replace("NAME_", ""), StringHandler.isValidDouble(properties.getProperty(propertySet.getSecond())) ? Double.parseDouble(properties.getProperty(propertySet.getSecond())) : Double.parseDouble(classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", "")).get(classReference).toString()));
                        } else if (propertySet.getThird().equals(Integer.TYPE) || propertySet.getThird().equals(int.class)) {
                            FileHandler.setField(classReference, propertySet.getFirst().replace("NAME_", ""), StringHandler.isValidInteger(properties.getProperty(propertySet.getSecond())) ? Integer.parseInt(properties.getProperty(propertySet.getSecond())) : Integer.parseInt(classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", "")).get(classReference).toString()));
                        } else {
                            // Additional Checks for Other Types - TODO
                            FileHandler.setField(classReference, propertySet.getFirst().replace("NAME_", ""), !StringHandler.isNullOrEmpty(properties.getProperty(propertySet.getSecond())) ? properties.getProperty(propertySet.getSecond()) : classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", "")).get(classReference).toString());
                        }

                        // Refresh Mappings and Properties
                        configValueMappings.set(currentIndex, new Quadruplet<>(propertySet.getFirst(), propertySet.getSecond(), propertySet.getThird(), classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", "")).get(classReference).toString()));
                        setProperty(propertySet.getSecond(), propertySet.getFourth());
                    }
                    currentIndex++;
                }

                // Execute all Mod Config Read Events, if any
                for (ConfigInstance configInstance : UniversalLib.configInstances) {
                    configInstance.onRead();
                }

                // Refresh Mappings based on new Values
            } catch (Exception ex) {
                verifyConfig();
            } finally {
                if (!verified) {
                    verifyConfig();
                }
                if (!skipLogging) {
                    FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.info.config.save", modID));
                }
            }
        }

        try {
            if (configReader != null) {
                configReader.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.dataclose"));
            ex.printStackTrace();
        }
    }

    public void updateConfig() {
        try {
            // Save Each Property from Mappings to Config Properties
            for (Quadruplet<String, String, ? extends Class<?>, String> propertySet : configValueMappings) {
                setProperty(propertySet.getSecond(), propertySet.getFourth());
            }

            // Execute All Mod Config Update Events, if any
            for (ConfigInstance configInstance : UniversalLib.configInstances) {
                configInstance.onUpdate();
            }

            // Check for Conflicts before Saving
            // None here for now. Yay!
        } catch (Exception ex) {
            // TODO
            ex.printStackTrace();
        } finally {
            save();
        }
    }

    private void verifyConfig() {
        boolean needsFullUpdate = false;

        for (Quadruplet<String, String, ? extends Class<?>, String> propertySet : configValueMappings) {
            if (!properties.stringPropertyNames().contains(propertySet.getSecond())) {
                FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.config.emptyprop", propertySet.getSecond()));
                Field configPropertyValue = null;
                try {
                    // Case 1: Try to Locate Property Value by Exact Case
                    // Ex: NAME_exampleValue tries to get from exampleValue Assignment
                    configPropertyValue = classReference.getClass().getDeclaredField(propertySet.getFirst().replace("NAME_", ""));
                } catch (Exception ex) {
                    // Case 2: Look through All Declared Fields to see If it matches in lower case
                    // Ex: NAME_exampleValue >> Tries to find exampleValue in any case
                    for (Field declaredField : classReference.getClass().getDeclaredFields()) {
                        if (declaredField != null && declaredField.getName().toLowerCase().equalsIgnoreCase(propertySet.getFirst().replace("NAME_", "").toLowerCase())) {
                            configPropertyValue = declaredField;
                            break;
                        }
                    }
                }

                try {
                    // Attempt to Save Value if Found a Matching Value
                    if (configPropertyValue != null) {
                        configPropertyValue.setAccessible(true);
                        setProperty(propertySet.getSecond(), propertySet.getFourth());
                        needsFullUpdate = true;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        for (String property : properties.stringPropertyNames()) {
            if (!FrameworkProperties.getListOfSeconds(configValueMappings).contains(property)) {
                FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.config.invalidprop", property));
                properties.remove(property);
                save();
            } else {
                // Execute All Mod Config Validation Events, if any
                for (ConfigInstance configInstance : UniversalLib.configInstances) {
                    configInstance.validationEvent();
                }
            }
        }

        if (!properties.stringPropertyNames().isEmpty()) {
            // Execute All Mod Config Transfer Events, if any
            for (ConfigInstance configInstance : UniversalLib.configInstances) {
                configInstance.transferEvent();
            }
        }

        if (needsFullUpdate) {
            setupInitialValues();
            verified = false;
            if (!properties.stringPropertyNames().isEmpty()) {
                read(true);
            }
            updateConfig();
        } else {
            verified = true;
            save();
        }
    }

    public void save() {
        Writer configWriter = null;
        FileOutputStream outputStream = null;

        try {
            outputStream = FileHandler.getOutputStream(configFile);
            configWriter = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
            properties.store(configWriter, null);
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.config.save"));
            ex.printStackTrace();
        }

        try {
            if (configWriter != null) {
                configWriter.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate(true, "universallib.logger.error.dataclose"));
            ex.printStackTrace();
        }
    }

    public void setProperty(final String propertyName, String propertyValue) {
        if (propertyValue.startsWith("translate") && propertyValue.replace("translate", "").replaceAll("[()]", "").split(",").length == 2) {
            // Translate Argument Handling
            String[] translationArgument = propertyValue.replace("translate", "").replaceAll("[()]", "").split(",", 2);
            if (translationArgument[0].equalsIgnoreCase("lib")) {
                propertyValue = FrameworkConstants.TRANSLATOR.translate(translationArgument[1].trim());
            } else {
                propertyValue = translatorInstance.translate(translationArgument[1].trim());
            }
        }

        properties.setProperty(propertyName, propertyValue);
        save();
    }
}
