package com.gitlab.cdagaming.universallib.config;

import com.gitlab.cdagaming.universallib.api.functions.Quadruplet;
import com.google.common.collect.Lists;

import java.util.List;

public class FrameworkProperties {
    // Config Names
    // ADVANCED
    public String NAME_enableCommands, NAME_renderTooltips, NAME_splitCharacter;
    // ACCESSIBILITY
    public String NAME_tooltipBGColor, NAME_tooltipBorderColor, NAME_guiBGColor, NAME_languageID, NAME_stripTranslationColors, NAME_showLoggingInChat;

    // Config Variables
    // ADVANCED
    public boolean enableCommands, renderTooltips;
    public String splitCharacter;
    // ACCESSIBILITY
    public String tooltipBGColor, tooltipBorderColor, guiBGColor, languageID;
    public boolean stripTranslationColors, showLoggingInChat;

    // Framework Methods used in Config API

    /**
     * Gets a List of the the unTranslated Field Names (NAME_enableCommands)
     *
     * @param originList The Original Quadruplet List
     * @return List of the First Variables in the Quadruplet Set List
     */
    public static List<?> getListOfFirsts(final List<Quadruplet<String, String, ? extends Class<?>, String>> originList) {
        List<Object> finalList = Lists.newArrayList();

        for (Quadruplet<?, ?, ?, ?> listSet : originList) {
            finalList.add(listSet.getFirst());
        }
        return finalList;
    }

    /**
     * Gets a List of the the translated Field Names (Enable Commands)
     *
     * @param originList The Original Quadruplet List
     * @return List of the Second Variables in the Quadruplet Set List
     */
    public static List<?> getListOfSeconds(final List<Quadruplet<String, String, ? extends Class<?>, String>> originList) {
        List<Object> finalList = Lists.newArrayList();

        for (Quadruplet<?, ?, ?, ?> listSet : originList) {
            finalList.add(listSet.getSecond());
        }
        return finalList;
    }

    /**
     * Gets a List of the Type of Value each config Value is (String, Boolean, Double, Long, etc)
     *
     * @param originList The Original Quadruplet List
     * @return List of the Third Variables in the Quadruplet Set List
     */
    public static List<?> getListOfThirds(final List<Quadruplet<String, String, ? extends Class<?>, String>> originList) {
        List<Object> finalList = Lists.newArrayList();

        for (Quadruplet<?, ?, ?, ?> listSet : originList) {
            finalList.add(listSet.getThird());
        }
        return finalList;
    }

    /**
     * Gets a List of the value of each config option (true/false, a string value, a number, etc)
     *
     * @param originList The Original Quadruplet List
     * @return List of the Fourth Variables in the Quadruplet Set List
     */
    public static List<?> getListOfFourths(final List<Quadruplet<String, String, ? extends Class<?>, String>> originList) {
        List<Object> finalList = Lists.newArrayList();

        for (Quadruplet<?, ?, ?, ?> listSet : originList) {
            finalList.add(listSet.getFourth());
        }
        return finalList;
    }
}
