package com.gitlab.cdagaming.universallib.handler;

import com.gitlab.cdagaming.universallib.FrameworkConstants;
import com.gitlab.cdagaming.universallib.UniversalLib;
import com.google.common.collect.Maps;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Map;

public class TranslationHandler {
    public static TranslationHandler instance;
    public boolean isUnicode = false;
    private String languageID = "en_US", modID;
    private Map<String, String> translationMap = Maps.newHashMap();
    private Map<String, Boolean> requestMap = Maps.newHashMap();
    private boolean usingJSON = false;

    public TranslationHandler() {
        setLanguage(UniversalLib.PROPERTIES != null ? UniversalLib.PROPERTIES.languageID : languageID);
        setUsingJSON(false);
        getTranslationMap();
        checkUnicode();
    }

    public TranslationHandler(final boolean useJSON) {
        setLanguage(UniversalLib.PROPERTIES != null ? UniversalLib.PROPERTIES.languageID : languageID);
        setUsingJSON(useJSON);
        getTranslationMap();
        checkUnicode();
    }

    public TranslationHandler(final String modID) {
        setLanguage(UniversalLib.PROPERTIES != null ? UniversalLib.PROPERTIES.languageID : languageID);
        setModID(modID);
        setUsingJSON(false);
        getTranslationMap();
        checkUnicode();
    }

    public TranslationHandler(final String modID, final boolean useJSON) {
        setLanguage(UniversalLib.PROPERTIES != null ? UniversalLib.PROPERTIES.languageID : languageID);
        setModID(modID);
        setUsingJSON(useJSON);
        getTranslationMap();
        checkUnicode();
    }

    public void tick() {
        if (UniversalLib.PROPERTIES != null && !languageID.equals(UniversalLib.PROPERTIES.languageID) &&
                (!requestMap.containsKey(UniversalLib.PROPERTIES.languageID) || requestMap.get(UniversalLib.PROPERTIES.languageID))) {
            setLanguage(UniversalLib.PROPERTIES.languageID);
            getTranslationMap();
            checkUnicode();
        }

        if (FrameworkConstants.minecraftInstance != null && FrameworkConstants.minecraftInstance.gameSettings != null && isUnicode != FrameworkConstants.minecraftInstance.gameSettings.forceUnicodeFont) {
            checkUnicode();
        }
    }

    private void checkUnicode() {
        isUnicode = false;
        int i = 0;
        int totalLength = 0;

        for (String currentString : translationMap.values()) {
            int currentLength = currentString.length();
            totalLength += currentLength;

            for (int index = 0; index < currentLength; ++index) {
                if (currentString.charAt(index) >= 256) {
                    ++i;
                }
            }
        }

        float f = (float) i / (float) totalLength;
        isUnicode = (double) f > 0.1D || (FrameworkConstants.minecraftInstance != null && FrameworkConstants.minecraftInstance.gameSettings != null && FrameworkConstants.minecraftInstance.gameSettings.forceUnicodeFont);
    }

    private void setUsingJSON(final boolean usingJSON) {
        this.usingJSON = usingJSON;
    }

    private void setLanguage(final String languageID) {
        if (!StringHandler.isNullOrEmpty(languageID)) {
            this.languageID = languageID;
        } else {
            this.languageID = "en_US";
        }
    }

    private void setModID(final String modID) {
        if (!StringHandler.isNullOrEmpty(modID)) {
            this.modID = modID;
        } else {
            this.modID = null;
        }
    }

    private void getTranslationMap() {
        translationMap = Maps.newHashMap();

        InputStream in = StringHandler.getResourceAsStream(TranslationHandler.class, "/assets/"
                + (!StringHandler.isNullOrEmpty(modID) ? modID + "/" : "") +
                "lang/" + languageID + (usingJSON ? ".json" : ".lang"));
        InputStream fallbackIn = StringHandler.getResourceAsStream(TranslationHandler.class, "/assets/"
                + (!StringHandler.isNullOrEmpty(modID) ? modID + "/" : "") +
                "lang/" + languageID.toLowerCase() + (usingJSON ? ".json" : ".lang"));

        if (in != null || fallbackIn != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in != null ? in : fallbackIn, Charset.forName("UTF-8")));
            try {
                String currentString;
                while ((currentString = reader.readLine()) != null) {
                    currentString = currentString.trim();
                    if (!currentString.startsWith("#") && !currentString.startsWith("[{}]") && (usingJSON ? currentString.contains(":") : currentString.contains("="))) {
                        String[] splitTranslation = usingJSON ? currentString.split(":", 2) : currentString.split("=", 2);
                        if (usingJSON) {
                            String str1 = splitTranslation[0].substring(1, splitTranslation[0].length() - 1).replace("\\n", "\n").replace("\\", "").trim();
                            String str2 = splitTranslation[1].substring(2, splitTranslation[1].length() - 2).replace("\\n", "\n").replace("\\", "").trim();
                            translationMap.put(str1, str2);
                        } else {
                            translationMap.put(splitTranslation[0].trim(), splitTranslation[1].trim());
                        }
                    }
                }

                if (in != null) {
                    in.close();
                }
                if (fallbackIn != null) {
                    fallbackIn.close();
                }
            } catch (Exception ex) {
                FrameworkConstants.LOG.error("An Exception has Occurred while Loading Translation Mappings, Things may not work well...");
                ex.printStackTrace();
            }
        } else {
            FrameworkConstants.LOG.error("Translations for " + modID + " do not exist for " + languageID);
            requestMap.put(languageID, false);
            setLanguage("en_US");
        }
    }

    public String translate(boolean stripColors, String translationKey, Object... parameters) {
        boolean hasError = false;
        String translatedString = translationKey;
        try {
            if (translationMap.containsKey(translationKey)) {
                translatedString = String.format(translationMap.get(translationKey), parameters);
            } else {
                hasError = true;
            }
        } catch (Exception ex) {
            FrameworkConstants.LOG.error("Exception Parsing " + translationKey);
            ex.printStackTrace();
            hasError = true;
        }

        if (hasError) {
            FrameworkConstants.LOG.error("Unable to retrieve a Translation for " + translationKey);
        }
        return stripColors ? StringHandler.stripColors(translatedString) : translatedString;
    }

    public String translate(String translationKey, Object... parameters) {
        return translate(UniversalLib.PROPERTIES != null && UniversalLib.PROPERTIES.stripTranslationColors, translationKey, parameters);
    }
}
