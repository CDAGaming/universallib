package com.gitlab.cdagaming.universallib.handler;

import com.gitlab.cdagaming.universallib.FrameworkConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class URLHandler {
    private static final String USER_AGENT = FrameworkConstants.MODID + "/" + FrameworkConstants.MCVersion;
    private static Gson GSON = new GsonBuilder().create();

    public static String getURLText(final URL url) throws Exception {
        final BufferedReader in = getURLReader(url);
        final StringBuilder response = new StringBuilder();
        String inputLine;
        while (!StringHandler.isNullOrEmpty((inputLine = in.readLine()))) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public static BufferedReader getURLReader(final String url) throws Exception {
        return getURLReader(new URL(url));
    }

    public static BufferedReader getURLReader(final URL url) throws Exception {
        return new BufferedReader(getURLStreamReader(url));
    }

    public static InputStream getURLStream(final URL url) throws Exception {
        final URLConnection connection = url.openConnection();
        if (!connection.getRequestProperties().containsKey("User-Agent")) {
            connection.addRequestProperty("User-Agent", USER_AGENT);
        }
        return (connection.getInputStream());
    }

    public static InputStreamReader getURLStreamReader(final URL url) throws Exception {
        return new InputStreamReader(getURLStream(url), Charset.forName("UTF-8"));
    }

    public static <T> T getJSONFromURL(String url, Class<T> classReference) throws Exception {
        return getJSONFromURL(new URL(url), classReference);
    }

    public static <T> T getJSONFromURL(URL url, Class<T> classReference) throws Exception {
        return GSON.fromJson(getURLStreamReader(url), classReference);
    }
}
