package com.gitlab.cdagaming.universallib.handler;

import com.gitlab.cdagaming.universallib.FrameworkConstants;
import com.gitlab.cdagaming.universallib.UniversalLib;
import com.gitlab.cdagaming.universallib.api.ModInstance;

public class SystemHandler {
    public int TIMER = 0;
    public String OS_NAME;
    public String OS_ARCH;
    public String USER_DIR;
    public boolean IS_LINUX = false, IS_MAC = false, IS_WINDOWS = false;
    public long CURRENT_TIMESTAMP;
    private boolean isTiming = false;
    private long BEGINNING_TIMESTAMP, ELAPSED_TIME;

    public SystemHandler() {
        try {
            OS_NAME = System.getProperty("os.name");
            OS_ARCH = System.getProperty("os.arch");
            USER_DIR = System.getProperty("user.dir");

            IS_LINUX = OS_NAME.startsWith("Linux") || OS_NAME.startsWith("LINUX");
            IS_MAC = OS_NAME.startsWith("Mac");
            IS_WINDOWS = OS_NAME.startsWith("Windows");
            CURRENT_TIMESTAMP = System.currentTimeMillis();
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate("craftpresence.logger.error.system"));
            ex.printStackTrace();
        }
    }

    public void tick() {
        ELAPSED_TIME = (System.currentTimeMillis() - CURRENT_TIMESTAMP) / 1000L;

        if (TIMER > 0) {
            if (!isTiming) {
                startTimer();
            } else {
                checkTimer();
            }
        }

        // Do Tasks based upon Events set in each ModInstance, if not ShutDown
        for (ModInstance modInstance : UniversalLib.modInstances) {
            if (!modInstance.isClosingOrHasError()) {
                for (int tickValue : modInstance.loopSecondEvents().keySet()) {
                    if (ELAPSED_TIME % tickValue == 0) {
                        modInstance.loopSecondEvents().get(tickValue).execute();
                    }
                }
            }
        }
    }

    private void startTimer() {
        BEGINNING_TIMESTAMP = System.currentTimeMillis() + (TIMER * 1000L);
        isTiming = true;
    }

    private void checkTimer() {
        if (TIMER > 0) {
            long remainingTime = (BEGINNING_TIMESTAMP - System.currentTimeMillis()) / 1000L;
            TIMER = (int) remainingTime;
        } else if (isTiming) {
            isTiming = false;
        }
    }
}
