package com.gitlab.cdagaming.universallib.handler;

import com.gitlab.cdagaming.universallib.FrameworkConstants;
import com.google.common.collect.Lists;
import com.google.common.reflect.ClassPath;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class FileHandler {
    private static Gson GSON = new GsonBuilder().create();

    public static <T> T getJSONFromFile(File file, Class<T> classObj) throws Exception {
        return getJSONFromFile(fileToString(file), classObj);
    }

    public static void downloadFile(final String urlString, final File file) {
        try {
            FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate("universallib.logger.info.download.init", file.getName(), file.getAbsolutePath(), urlString));
            final URL url = new URL(urlString);
            if (file.exists()) {
                final boolean fileDeleted = file.delete();
                if (!fileDeleted) {
                    FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate("universallib.logger.error.delete.file", file.getName()));
                }
            }

            FileUtils.copyURLToFile(url, file);
            FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate("universallib.logger.info.download.loaded", file.getName(), file.getAbsolutePath(), urlString));
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate("universallib.logger.error.download", file.getName(), urlString, file.getAbsolutePath()));
            ex.printStackTrace();
        }
    }

    public static void loadFileAsDLL(final File file) {
        try {
            FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate("universallib.logger.info.dll.init", file.getName()));
            boolean isPermsSet = file.setReadable(true) && file.setWritable(true);
            if (isPermsSet) {
                System.load(file.getAbsolutePath());
            }
            FrameworkConstants.LOG.info(FrameworkConstants.TRANSLATOR.translate("universallib.logger.info.dll.loaded", file.getName()));
        } catch (Exception ex) {
            FrameworkConstants.LOG.error(FrameworkConstants.TRANSLATOR.translate("universallib.logger.error.dll", file.getName()));
            ex.printStackTrace();
        }
    }

    public static FileOutputStream getOutputStream(final File fileInstance) {
        FileOutputStream result = null;

        try {
            if (fileInstance.exists() || (fileInstance.getParentFile().mkdirs() && fileInstance.createNewFile()) || fileInstance.createNewFile()) {
                result = new FileOutputStream(fileInstance.getAbsoluteFile());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static <T> T getJSONFromFile(String file, Class<T> clazz) {
        return GSON.fromJson(file, clazz);
    }

    public static String fileToString(File file) throws Exception {
        return FileUtils.readFileToString(file, Charset.forName("UTF-8"));
    }

    public static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return name.substring(lastIndexOf);
    }

    public static int getModCount() {
        int modCount = 0;
        final File[] mods = new File(FrameworkConstants.modsDir).listFiles();

        if (mods != null) {
            for (File modFile : mods) {
                if (getFileExtension(modFile).equals(".jar")) {
                    modCount++;
                }
            }
        }
        return modCount;
    }

    @SuppressWarnings("UnstableApiUsage")
    public static List<Class<?>> getClassNamesMatchingSuperType(final List<Class<?>> searchList, final String... sourcePackages) {
        final List<Class<?>> matchingClasses = Lists.newArrayList(), availableClassList = Lists.newArrayList();
        final List<ClassPath.ClassInfo> classList = Lists.newArrayList();
        final List<String> sourceList = Lists.newArrayList(sourcePackages);

        // Attempt to Get Top Level Classes from the JVM Class Loader
        try {
            classList.addAll(ClassPath.from(FrameworkConstants.CLASS_LOADER).getTopLevelClasses());
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        for (ClassPath.ClassInfo classInfo : classList) {
            // Attempt to Add Classes Matching any of the Source Packages
            if ((sourceList.contains(classInfo.getName()) || (sourceList.isEmpty() &&
                    !classInfo.getName().contains("FMLServerHandler")
                    && !classInfo.getName().toLowerCase().contains("lwjgl")
                    && !classInfo.getName().toLowerCase().contains("com.mojang")
                    && !classInfo.getName().toLowerCase().contains("fastutil")
                    && !classInfo.getName().toLowerCase().contains("net.java")
                    && !classInfo.getName().toLowerCase().contains("javax")
                    && !classInfo.getName().toLowerCase().contains("jorbis")
                    && !classInfo.getName().toLowerCase().contains("apache")
                    && !classInfo.getName().toLowerCase().contains("ibxm")
                    && !classInfo.getName().toLowerCase().contains("net.minecraft")
                    && !classInfo.getName().toLowerCase().contains("mixin")))) {
                try {
                    Class classObj = Class.forName(classInfo.getName());
                    availableClassList.add(classObj);
                    for (Class subClassObj : classObj.getClasses()) {
                        if (!availableClassList.contains(subClassObj)) {
                            availableClassList.add(subClassObj);
                        }
                    }
                } catch (Exception ignored) {
                    // Ignore this Exception and Continue
                } catch (Error ignored) {
                    // Ignore this Error and Continue
                }
            }
        }

        for (Class<?> classObj : availableClassList) {
            Class currentClassObj = classObj;
            List<Class<?>> superClassList = Lists.newArrayList();

            // Add All Interfaces of this Class to Matching List
            for (Class<?> interfaceClass : currentClassObj.getInterfaces()) {
                if (searchList.contains(interfaceClass)) {
                    matchingClasses.add(currentClassObj);
                }
            }

            // Add All SuperClasses of this Class to the List as well
            while (currentClassObj.getSuperclass() != null && !searchList.contains(currentClassObj.getSuperclass())) {
                superClassList.add(currentClassObj.getSuperclass());
                currentClassObj = currentClassObj.getSuperclass();
            }

            // If Match is Found, add original Class to final List, and add all Super Classes to returning List
            if (currentClassObj.getSuperclass() != null && searchList.contains(currentClassObj.getSuperclass())) {
                matchingClasses.add(classObj);
                matchingClasses.addAll(superClassList);
            }
        }

        // Attempt to Retrieve Mod Classes
        for (String modClassString : getModClassNames()) {
            Class modClassObj, currentClassObj;
            List<Class<?>> superClassList = Lists.newArrayList();

            if (!modClassString.toLowerCase().contains("mixin")) {
                try {
                    modClassObj = Class.forName(modClassString);
                    currentClassObj = modClassObj;

                    if (modClassObj != null) {
                        // Add All Interfaces of this Class to Matching List
                        for (Class<?> interfaceClass : currentClassObj.getInterfaces()) {
                            if (searchList.contains(interfaceClass)) {
                                matchingClasses.add(currentClassObj);
                            }
                        }

                        // Add all SuperClasses of Mod Class to a List
                        while (currentClassObj.getSuperclass() != null && !searchList.contains(currentClassObj.getSuperclass())) {
                            superClassList.add(currentClassObj.getSuperclass());
                            currentClassObj = currentClassObj.getSuperclass();
                        }

                        // If Match is Found, add original Class to final List, and add all Super Classes to returning List
                        if (currentClassObj.getSuperclass() != null && searchList.contains(currentClassObj.getSuperclass())) {
                            matchingClasses.add(modClassObj);
                            matchingClasses.addAll(superClassList);
                        }
                    }
                } catch (Exception ignored) {
                    // Ignore this Exception and Continue
                } catch (Error ignored) {
                    // Ignore this Error and Continue
                }
            }
        }
        return matchingClasses;
    }

    public static List<Class<?>> getClassNamesMatchingSuperType(final Class<?> searchTarget, final String... sourcePackages) {
        List<Class<?>> searchClasses = Lists.newArrayList();
        searchClasses.add(searchTarget);

        return getClassNamesMatchingSuperType(searchClasses, sourcePackages);
    }

    public static List<Class<?>> getClassNamesMatchingSuperType(final Class<?>[] searchTargets, final String... sourcePackages) {
        List<Class<?>> searchClasses = Lists.newArrayList();
        searchClasses.addAll(Arrays.asList(searchTargets));

        return getClassNamesMatchingSuperType(searchClasses, sourcePackages);
    }

    public static boolean setField(Object targetObject, String fieldName, Object fieldValue) {
        Field field;
        try {
            field = targetObject.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            field = null;
        }
        Class superClass = targetObject.getClass().getSuperclass();
        while (field == null && superClass != null) {
            try {
                field = superClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                superClass = superClass.getSuperclass();
            }
        }
        if (field == null) {
            return false;
        }
        field.setAccessible(true);
        try {
            if (field.getGenericType().equals(Boolean.TYPE)) {
                field.set(targetObject, Boolean.parseBoolean(String.valueOf(fieldValue)));
            } else if (field.getGenericType().equals(Double.TYPE)) {
                field.set(targetObject, Double.parseDouble(String.valueOf(fieldValue)));
            } else if (field.getGenericType().equals(Integer.TYPE)) {
                field.set(targetObject, Integer.parseInt(String.valueOf(fieldValue)));
            } else if (field.getGenericType().equals(String[].class)) {
                field.set(targetObject, String.valueOf(fieldValue).replaceAll("\\[(.*?)]", "$1").split(","));
            } else {
                field.set(targetObject, fieldValue);
            }
            return true;
        } catch (IllegalAccessException e) {
            return false;
        }
    }

    public static List<String> getModClassNames() {
        List<String> classNames = Lists.newArrayList();
        final File[] mods = new File(FrameworkConstants.modsDir).listFiles();

        if (mods != null) {
            for (File modFile : mods) {
                if (getFileExtension(modFile).equals(".jar")) {
                    try {
                        JarFile jarFile = new JarFile(modFile.getAbsolutePath());
                        Enumeration allEntries = jarFile.entries();
                        while (allEntries.hasMoreElements()) {
                            JarEntry entry = (JarEntry) allEntries.nextElement();
                            String file = entry.getName();
                            if (file.endsWith(".class")) {
                                String className = file.replace('/', '.').substring(0, file.length() - 6);
                                classNames.add(className);
                            }
                        }
                        jarFile.close();
                    } catch (Exception ignored) {
                    }
                }
            }
            return classNames;
        } else {
            return Collections.emptyList();
        }
    }
}
